Encrypted samba automount
=========================
Creates encrypted automounted samba volume from remote server (created using [samba-server] ansible role). Check this role [samba-server] for reasoning why this must be done this way.

Requirements
------------

Remote server volumes created with [samba-server].

Role Variables
--------------

Check [defaults/main.yml] for more informations.

Example Playbook
----------------

Check [tests/main.yml] for example playbook:

    - hosts: all
      tasks:
        include_role:
            name: piMediaServer/samba-client
        vars:
            SAMBA_REMOTE_MOUNT: 'name-of-remote-mount'
            STUNNEL_REMOTE_HOST: 'example.com'
            STUNNEL_REMOTE_HOST_PORT: '5000'

Command line usage:

    $ export ROLES_DIR=$HOME/git/ansible/roles
    $ export ANSIBLE_CONFIG=$ROLES_DIR/ansible.cfg
    $ ansible-playbook $ROLES_DIR/piMediaServer/samba-server/tests/main.yml \
        -e SAMBA_REMOTE_MOUNT: 'name-of-remote-mount' \
        -e STUNNEL_REMOTE_HOST=example.com \
        -e STUNNEL_REMOTE_HOST_PORT=5000

License
-------

BSD

Author Information
------------------

Written by: [Michal Nováček]


[Michal Nováček]: mailto:michal.novacek@gmail.com
[defaults/main.yml]: defaults/main.yml
[samba-server]: http://gitlab.com/misacek/samba-server
[tests/main.yml]: tests/main.yml
